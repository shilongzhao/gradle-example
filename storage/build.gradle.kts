plugins {
	id("gradle.acme.kotlin-library-conventions")
//    id("com.avast.gradle.docker-compose") version "0.14.0"
    id("org.flywaydb.flyway") version "8.2.2"
}



dependencies {
    api( "org.flywaydb:flyway-core:8.2.2")
    runtimeOnly("org.postgresql:postgresql:42.2.14")
}


flyway {
    url = "jdbc:postgresql://localhost:5431/bankaccountdb"
    user = "testuser"
    password = "testpass"
}

tasks.named<org.flywaydb.gradle.task.FlywayMigrateTask>("flywayMigrate") {
//    dependsOn("composeUp")
}
