CREATE TABLE account_holder(
   id         UUID NOT NULL PRIMARY KEY,
   user_name  TEXT NOT NULL UNIQUE NOT NULL,
   first_name TEXT NOT NULL,
   last_name  TEXT NOT NULL,
   record_version BIGINT NOT NULL DEFAULT 1
);
CREATE INDEX account_holder_user_name_idx on account_holder(user_name);

CREATE TYPE bank_account_type AS ENUM ('CHECKING', 'SAVINGS');
CREATE TABLE bank_account(
 id                UUID         NOT NULL PRIMARY KEY,
 account_holder_id UUID            REFERENCES account_holder ON DELETE CASCADE,
 account_type      bank_account_type NOT NULL
);
CREATE INDEX bank_account_account_holder_id_idx on bank_account(account_holder_id);

CREATE TYPE transaction_type AS ENUM ('DEPOSIT', 'WITHDRAWAL');
CREATE TABLE account_transaction(
    id               UUID        PRIMARY KEY,
    bank_account_id  UUID REFERENCES bank_account ON DELETE CASCADE,
    amount           NUMERIC(12, 2)   NOT NULL,
    transaction_type transaction_type NOT NULL
);
CREATE INDEX account_transaction_bank_account_id_idx on account_transaction(bank_account_id);
