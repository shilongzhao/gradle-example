import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
  id("com.github.johnrengelman.shadow") version "7.1.1"
  id("gradle.acme.kotlin-application-conventions")
    kotlin("kapt")
}

val vertxVersion = "4.2.6"
val prometheusVersion = "1.8.1"

val mainVerticleName = "com.acme.MainVerticle"
val launcherClassName = "io.vertx.core.Launcher"

val watchForChange = "src/**/*"
val doOnChange = "${projectDir}/gradlew classes"

application {
  mainClass.set(launcherClassName)
}

dependencies {
  implementation(project(":storage"))
  implementation(platform("io.vertx:vertx-stack-depchain:$vertxVersion"))
  implementation("io.vertx:vertx-pg-client")

  implementation("io.vertx:vertx-service-proxy")
  compileOnly("io.vertx:vertx-codegen:$vertxVersion")
  kapt("io.vertx:vertx-codegen:$vertxVersion:processor")

  implementation("io.vertx:vertx-config")

  implementation("io.arrow-kt:arrow-core:1.0.1")
  implementation("io.vertx:vertx-web")

  implementation("io.vertx:vertx-rabbitmq-client")

  implementation("io.vertx:vertx-lang-kotlin")
  implementation("io.vertx:vertx-lang-kotlin-coroutines")

  implementation("io.vertx:vertx-micrometer-metrics")
  implementation("io.micrometer:micrometer-registry-prometheus:$prometheusVersion")

  implementation("io.vertx:vertx-web-api-contract")
  implementation("io.vertx:vertx-lang-kotlin")
  implementation(kotlin("stdlib-jdk8"))
  testImplementation("io.vertx:vertx-junit5")
}



tasks.withType<ShadowJar> {
  archiveClassifier.set("fat")
  manifest {
    attributes(mapOf("Main-Verticle" to mainVerticleName))
  }
  mergeServiceFiles()
}

tasks.withType<Test> {
  useJUnitPlatform()
  testLogging {
    events = setOf(PASSED, SKIPPED, FAILED)
  }
}

tasks.withType<JavaExec> {
  args = listOf("run", mainVerticleName, "--redeploy=$watchForChange", "--launcher-class=$launcherClassName", "--on-redeploy=$doOnChange")
}

sourceSets {
  main {
    java {
      srcDirs(project.file("${project.buildDir}/generated/main/java"))
    }
  }
}
