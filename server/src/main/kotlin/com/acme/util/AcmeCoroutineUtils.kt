package com.acme.util

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import arrow.core.left
import arrow.core.right
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

/**
 * A function returning a Future[T] is equivalent to a suspend function returning
 * an Either[Throwable, T], since a future could be either a failed future or a
 * successful future
 */
/**
 * transform async function to suspend function
 */
fun <T> suspension(block: () -> Future<T>): suspend () -> Either<Throwable, T> = suspend {
    try {
        block().await().right()
    } catch (e: Exception) {
        e.left()
    }
}

suspend fun <T> Future<T>.either(): Either<Throwable, T> =
    try {
        await().right()
    } catch (e: Throwable) {
        e.left()
    }

/**
 * transform suspend function to async function
 */
fun <T> CoroutineScope.future(block: suspend () -> Either<Throwable, T>): () -> Future<T> = {

    val p = Promise.promise<T>()
    launch {
        try {
            when (val either = block()) {
                is Right -> p.complete(either.value)
                is Left -> p.fail(either.value)
            }
        } catch (e: Exception) {
            p.fail(e)
        }
    }
    p.future()
}
