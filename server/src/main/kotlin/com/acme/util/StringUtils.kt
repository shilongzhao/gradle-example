package com.acme.util

import java.util.*

val UUID.string: String
    get() = this.toString()

val String.uuid: UUID
    get() = UUID.fromString(this)


