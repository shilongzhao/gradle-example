package com.acme

import io.vertx.ext.web.RoutingContext

typealias RoutingContextHandler = suspend  (RoutingContext) -> Unit
