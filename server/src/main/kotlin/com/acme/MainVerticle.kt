package com.acme

import com.acme.amqp.RabbitClientVerticle
import com.acme.database.PostgresClientVerticle
import com.acme.http.HttpServerVerticle
import com.acme.rabbit.RabbitPlainClientVerticle
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.apache.logging.log4j.kotlin.Logging

class MainVerticle : CoroutineVerticle(), Logging {

    override suspend fun start() {

        val appConfig = AppConfig.appConfig(vertx)
        logger.info { "app configuration: ${appConfig.encode()}" }

        // Deploy without embedded server: we need to "manually" expose the prometheus metrics
        vertx.deployVerticle(PostgresClientVerticle(), deploymentOptionsOf(config = appConfig)).await()
        vertx.deployVerticle(HttpServerVerticle()).await()
        vertx.deployVerticle(RabbitClientVerticle()).await()
        vertx.deployVerticle(RabbitPlainClientVerticle()).await()
        logger.info { "application started!" }
    }

}
