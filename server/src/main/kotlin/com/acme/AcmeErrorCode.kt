package com.acme

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.serviceproxy.ServiceException


enum class AcmeErrorCode(val code: Int, val message: String) {
    INVALID_TRANSFER_AMOUNT(0x1, "invalid transfer amount")
}

val ServiceException.json: JsonObject
    get() = jsonObjectOf(
        "failureCode" to failureCode(),
        "message" to message,
        "debugInfo" to debugInfo
    )

val ServiceException.jsonString: String
    get() = this.json.encode()
