package com.acme.database.pojo

import com.acme.util.uuid
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape
import io.vertx.codegen.annotations.DataObject
import io.vertx.core.json.JsonObject
import java.math.BigDecimal
import java.util.*

// we cannot use the jooq generated pojo, since we have to add custom annotations for vert.x eventbus serialisation
@DataObject
data class BankAccount(
    val id: UUID,
    val accountHolderId: UUID,
    @field: JsonFormat(shape = Shape.STRING)
    val accountType: BankAccountType,
    @field: JsonFormat(shape = Shape.STRING)
    val balance: BigDecimal,
    val recordVersion: Long) {

    fun toJson(): JsonObject = JsonObject.mapFrom(this)

    constructor(json: JsonObject) : this(
        json.getString("id").uuid,
        json.getString("accountHolderId").uuid,
        BankAccountType.valueOf(json.getString("accountType")),
        BigDecimal(json.getString("balance")),
        json.getLong("recordVersion")
    )

}

val BankAccount.json: JsonObject
    get() = toJson()

val BankAccount.jsonString: String
    get() = json.encode()

enum class BankAccountType {
    PRIVATE,
    SAVINGS
}
