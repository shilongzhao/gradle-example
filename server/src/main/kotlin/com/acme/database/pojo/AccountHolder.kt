package com.acme.database.pojo

import com.acme.util.uuid
import io.vertx.codegen.annotations.DataObject
import io.vertx.core.json.JsonObject
import java.util.*

@DataObject
class AccountHolder(
    val id: UUID,
    val username: String,
    val firstname: String,
    val lastname: String
) {
    constructor(json: JsonObject): this(
        json.getString("id").uuid,
        json.getString("username"),
        json.getString("firstname"),
        json.getString("lastname")
    )

    fun toJson(): JsonObject = JsonObject.mapFrom(this)
}
