package com.acme.database.pojo

import com.acme.util.uuid
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.*
import io.vertx.codegen.annotations.DataObject
import io.vertx.core.json.JsonObject
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@DataObject
data class BankTransfer(
    val id: UUID,
    val senderId: UUID,
    val receiverId: UUID,
    @field: JsonFormat(shape = Shape.STRING)
    val amount: BigDecimal,
    @field: JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    val timestamp: OffsetDateTime) {

    fun toJson(): JsonObject = JsonObject.mapFrom(this)

    constructor(json: JsonObject): this (
        json.getString("id").uuid,
        json.getString("sender").uuid,
        json.getString("receiver").uuid,
        BigDecimal(json.getString("amount")),
        OffsetDateTime.parse(json.getString("timestamp"), DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    )

}
