package com.acme.database

import com.acme.AppConfig
import com.acme.AppConfig.appConfig
import com.acme.AppConfig.getConnectionOptions
import com.acme.database.service.AccountHolderServiceVerticle
import com.acme.database.service.BankAccountServiceVerticle
import com.acme.database.service.BankTransferServiceVerticle
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.pgclient.PgPool
import org.apache.logging.log4j.kotlin.Logging

class PostgresClientVerticle: CoroutineVerticle(), Logging {

    override suspend fun start() {

        val pool = PgPool.pool(vertx, getConnectionOptions(config), poolOptionsOf(maxSize = 10))

        vertx.deployVerticle({ BankAccountServiceVerticle(pool) }, deploymentOptionsOf(instances = 2)).await()
        vertx.deployVerticle({ AccountHolderServiceVerticle(pool) }, deploymentOptionsOf(instances = 2)).await()
        vertx.deployVerticle({ BankTransferServiceVerticle(pool) }, deploymentOptionsOf(instances = 3)).await()

        logger.info { "deploy OK" }
    }

}
