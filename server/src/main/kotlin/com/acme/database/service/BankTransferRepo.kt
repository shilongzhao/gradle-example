package com.acme.database.service

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.acme.database.pojo.BankTransfer
import com.acme.database.service.BankTransferQueries.addValue
import com.acme.database.service.BankTransferQueries.deduceValue
import com.acme.database.service.BankTransferQueries.save
import com.acme.database.service.BankTransferRepoProxy.INVALID_AMOUNT
import com.acme.database.service.BankTransferRepoProxy.NOT_ENOUGH_BALANCE
import com.acme.util.future
import com.acme.util.string
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.codegen.annotations.VertxGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import io.vertx.pgclient.PgPool
import io.vertx.serviceproxy.ServiceBinder
import io.vertx.serviceproxy.ServiceException
import io.vertx.sqlclient.SqlClient
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.TransactionRollbackException
import io.vertx.sqlclient.Tuple
import kotlinx.coroutines.CoroutineScope
import org.apache.logging.log4j.kotlin.Logging
import java.math.BigDecimal
import java.util.*

@ProxyGen
@VertxGen
interface BankTransferRepo {
    fun transfer(transfer: BankTransfer): Future<String>
}


object BankTransferRepoProxy {
    const val ADDRESS = "com.acme.eventbus.services.bank_transfer"
    fun createProxy(vertx: Vertx): BankTransferRepo = BankTransferRepoVertxEBProxy(vertx, ADDRESS)

    val INVALID_AMOUNT = { value: BigDecimal -> ServiceException(-1, "Invalid amount $value") }
    val NOT_ENOUGH_BALANCE = { value: BigDecimal -> ServiceException(-1, "Balance not enough $value") }
}

object BankTransferQueries {
    internal fun addValue(id: UUID, value: BigDecimal, version: Long): (SqlClient) -> Future<String> = query@{ client ->
        if (value < BigDecimal.ZERO)
            return@query Future.failedFuture(INVALID_AMOUNT(value))

        client.preparedQuery(
            """  UPDATE bank_account  
                    |SET balance = balance + $1, 
                    |    record_version = record_version + 1  
                    |WHERE id = $2 
                    |    AND record_version = $3  
                    |RETURNING id """.trimMargin()
        )
            .execute(Tuple.of(value, id, version))
            .map { rs -> rs.first().getUUID("id").toString() }
    }

    internal fun deduceValue(id: UUID, value: BigDecimal, version: Long): (SqlClient) -> Future<String> =
        query@{ client ->
            if (value < BigDecimal.ZERO)
                return@query Future.failedFuture(INVALID_AMOUNT(value))

            client.preparedQuery(
                """  UPDATE bank_account  
                    |SET balance = balance - $1, 
                    |    record_version = record_version + 1  
                    |WHERE id = $2 
                    |    AND record_version = $3  
                    |RETURNING id """.trimMargin()
            )
                .execute(Tuple.of(value, id, version))
                .map { rs -> rs.first().getUUID("id").toString() }
        }

    internal fun save(bankTransfer: BankTransfer): (SqlClient) -> Future<String> = {
        Future.succeededFuture(bankTransfer.id.string)
    }
}

class BankTransferRepoImpl(private val vertx: Vertx, private val pool: PgPool) : BankTransferRepo, Logging {

    /**
     * Vertx proxy does not support suspend function,
     * so we have to use normal function with future return type
     */
    override fun transfer(transfer: BankTransfer): Future<String> =
        CoroutineScope(vertx.dispatcher())
            .future {
                transferSuspend(transfer)
            }
            .invoke()

    /**
     * suspend functions could be very useful if we are facing several chained async processes, using await() makes
     * our code looks easier to read.
     *
     * meanwhile, it's doubtful to use suspend functions for just simple one-shot async queries (like get account by id)
     */
    private suspend fun transferSuspend(transfer: BankTransfer): Either<Throwable, String> {
        if (transfer.amount <= BigDecimal.ZERO)
            return INVALID_AMOUNT(transfer.amount).left()

        val accountRepo = BankAccountRepoProxy.createProxy(vertx)
        val sender = accountRepo.getById(transfer.senderId.toString()).await()
        val receiver = accountRepo.getById(transfer.receiverId.toString()).await()

        if (sender.balance < transfer.amount) {
            return NOT_ENOUGH_BALANCE(transfer.amount).left()
        }

        return pool.withSuspendTransaction { conn ->
            val amount = transfer.amount

            receiver.apply { conn.execute(addValue(id, amount, recordVersion)).await() }

            sender.apply { conn.execute(deduceValue(id, amount, recordVersion)).await() }

            conn.execute(save(transfer)).await()

            transfer.id.toString()
        }
    }

    /**
    Here it's probably better to define query as type `(SqlConnection) -> Future<T>`
    since a query may fail or return the data, and future can be a failure or a result.

    or define the query as type `suspend (SqlConnection) -> Either<Throwable, T>` this way we can
    also keep the fact that a query may fail.

    And a result of a transaction could be a failure or a success, so here probably the return type
    should be Either<Throwable, T>

    or use Pool.withTransaction
     */
    private suspend fun <T> PgPool.withSuspendTransaction(query: suspend (SqlConnection) -> T): Either<Throwable, T> {

        val connection = this.connection.await()
        val transaction = try {
            connection.begin().await()
        } catch (e: Exception) {
            connection.close()
            return e.left()
        }

        return try {
            val result = query(connection)
            transaction.commit().await()
            result.right()
        } catch (e: Exception) {
            logger.warn(e) { "exception in transaction" }
            when (e) {
                is TransactionRollbackException -> e.left()
                else -> {
                    transaction.rollback().await()
                    e.left()
                }
            }
        } finally {
            connection.close().await()
        }
    }


    /*
    // implementation with asynchronous style without coroutines
    private fun transfer(sender: UUID, receiver: UUID, amount: BigDecimal): Future<BigDecimal> {
        return jdbcPool.withTransaction { connection ->
            getById(sender)(connection).flatMap { senderAccount ->
                getById(receiver)(connection).flatMap { receiverAccount ->
                    addValue(sender, amount.negate(), senderAccount.recordVersion)(connection).flatMap { updatedSender ->
                        if (updatedSender != 1) Future.failedFuture(ConcurrentModificationException())
                        else addValue(receiver, amount, receiverAccount.recordVersion)(connection).flatMap { updatedReceiver ->
                            if (updatedReceiver != 1) Future.failedFuture(ConcurrentModificationException())
                            else Future.succeededFuture(amount)
                        }
                    }
                }
            }
        }
    }
   */
}

class BankTransferServiceVerticle(private val pgPool: PgPool) : CoroutineVerticle(), Logging {
    override suspend fun start() {
        ServiceBinder(vertx)
            .setAddress(BankTransferRepoProxy.ADDRESS)
            .register(
                BankTransferRepo::class.java,
                BankTransferRepoImpl(vertx, pgPool)
            )
        logger.info { "deploy OK" }
    }
}
