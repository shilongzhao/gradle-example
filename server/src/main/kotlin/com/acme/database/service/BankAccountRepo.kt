package com.acme.database.service

import com.acme.database.pojo.BankAccount
import com.acme.database.pojo.BankAccountType
import com.acme.util.string
import com.acme.util.uuid
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.codegen.annotations.VertxGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.pgclient.PgPool
import io.vertx.serviceproxy.ServiceBinder
import io.vertx.sqlclient.Row
import io.vertx.sqlclient.Tuple
import org.apache.logging.log4j.kotlin.Logging
import java.util.*

@ProxyGen
@VertxGen
interface BankAccountRepo {
    fun insert(account: BankAccount): Future<String>
    fun getById(id: String): Future<BankAccount>
    fun batchInsert(accounts: List<BankAccount>): Future<List<String>>
}


object BankAccountRepoProxy {
    const val ADDRESS = "com.acme.eventbus.service.bank_account"
    fun createProxy(vertx: Vertx): BankAccountRepo = BankAccountRepoVertxEBProxy(vertx, ADDRESS)
}

object BankAccountQueries {
    /* it could be annoying to keep sync with the BankAccount and the string literal queries */
    private fun BankAccount.toTuple() =
        Tuple.of(id, accountHolderId, accountType, balance, recordVersion)

    private fun Row.toBankAccount(): BankAccount = BankAccount(
        getUUID("id"),
        getUUID("account_holder_id"),
        BankAccountType.valueOf(getString("account_type")),
        getBigDecimal("balance"),
        getLong("record_version")
    )

    internal fun insert(account: BankAccount): AcmeQuery<String> = { client ->
        client.preparedQuery(
            " INSERT INTO bank_account(id, account_holder_id, account_type, balance, record_version) " +
                    " VALUES ($1, $2, $3::bank_account_type, $4, $5) RETURNING id "
        )
            .execute(account.toTuple())
            .map {
                it.first().getUUID("id").string
            }
    }


    internal fun batchInsert(accounts: List<BankAccount>): AcmeQuery<List<String>> = { client ->
        client.preparedQuery(
            " INSERT INTO bank_account(id, account_holder_id, account_type, balance, record_version) " +
                    " VALUES ($1, $2, $3::bank_account_type, $4, $5) RETURNING id "
        ).executeBatch(
            accounts.map { it.toTuple() }
        ).map { rs ->
            rs.map { r ->
                r.getUUID("id").toString()
            }
        }
    }

    internal fun getById(id: UUID): AcmeQuery<BankAccount> = { client ->
        client
            .preparedQuery("""SELECT * FROM bank_account WHERE id = $1""")
            .execute(Tuple.of(id))
            .map {
                it.first().toBankAccount()
            }
    }
}

class BankAccountRepoImpl(vertx: Vertx, private val databaseContext: PgPool) :
    BankAccountRepo, Logging {

    override fun insert(account: BankAccount): Future<String> {
        return BankAccountQueries.insert(account)(databaseContext)
    }

    override fun getById(id: String): Future<BankAccount> {
        return BankAccountQueries.getById(id.uuid)(databaseContext)
    }

    override fun batchInsert(accounts: List<BankAccount>): Future<List<String>> {
        return BankAccountQueries.batchInsert(accounts)(databaseContext)
    }
}

class BankAccountServiceVerticle(private val pgPool: PgPool) : CoroutineVerticle(), Logging {
    override suspend fun start() {
        ServiceBinder(vertx)
            .setAddress(BankAccountRepoProxy.ADDRESS)
            .register(BankAccountRepo::class.java, BankAccountRepoImpl(vertx, pgPool))

        logger.info { "deploy OK" }
    }
}
