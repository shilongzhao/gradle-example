package com.acme.database.service

import com.acme.database.pojo.AccountHolder
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.codegen.annotations.VertxGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.pgclient.PgPool
import io.vertx.serviceproxy.ServiceBinder
import io.vertx.sqlclient.Row
import io.vertx.sqlclient.Tuple
import org.apache.logging.log4j.kotlin.Logging

@ProxyGen
@VertxGen
interface AccountHolderRepo {
    fun insert(holder: AccountHolder): Future<String>
    fun getById(id: String): Future<AccountHolder>
}

object AccountHolderRepoProxy {
    const val ADDRESS = "com.acme.eventbus.service.account_holder"
    fun createProxy(vertx: Vertx): AccountHolderRepo = AccountHolderRepoVertxEBProxy(vertx, ADDRESS)
}

object AccountHolderQueries {
    private fun AccountHolder.toTuple(): Tuple =
        Tuple.of(id, username, firstname, lastname)

    private fun Row.toAccountHolder(): AccountHolder =
        AccountHolder(id = getUUID("id"),
            username = getString("user_name"),
            firstname = getString("first_name"),
            lastname = getString("last_name"))

    fun insert(accountHolder: AccountHolder): AcmeQuery<String> = { client ->
        val tuple = accountHolder.toTuple()
        client.preparedQuery(
            """  INSERT INTO account_holder(id, user_name, first_name, last_name) 
                | VALUES ($1,$2,$3,$4) 
                | RETURNING id """.trimMargin())
            .execute(tuple)
            .map {
                it.first().getUUID("id").toString()
            }
    }

    fun getById(id: String): AcmeQuery<AccountHolder> = { client ->
        client.preparedQuery(
            """SELECT id, user_name, first_name, last_name 
                |FROM account_holder 
                |WHERE id = $1""".trimMargin())
            .execute(Tuple.of(id))
            .map { rs ->
                rs.first().toAccountHolder()
            }
    }
}
/**
 * no suspend function is used in this service, they are totally in an async way
 */
class AccountHolderRepoImpl(vertx: Vertx, private val pgPool: PgPool) :
    AccountHolderRepo, Logging {

    override fun insert(holder: AccountHolder): Future<String> {
        return AccountHolderQueries.insert(holder)(pgPool)
    }

    override fun getById(id: String): Future<AccountHolder> {
        return AccountHolderQueries.getById(id)(pgPool)
    }

}

class AccountHolderServiceVerticle(private val pgPool: PgPool): CoroutineVerticle(), Logging {
    override suspend fun start() {
        ServiceBinder(vertx)
            .setAddress(AccountHolderRepoProxy.ADDRESS)
            .register(AccountHolderRepo::class.java, AccountHolderRepoImpl(vertx, pgPool))
        logger.info { "deploy OK" }
    }
}
