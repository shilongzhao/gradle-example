package com.acme.database.service

import io.vertx.core.Future
import io.vertx.sqlclient.SqlClient

/*
  Since the parameter type here is SqlClient, you're free to choose between
  - SqlConnection, when you need controlled transaction
  - PgPool, when you do not care about transaction and want connection to be auto managed
 */
typealias AcmeQuery<T> = (SqlClient) -> Future<T>

fun <T> SqlClient.execute(query: AcmeQuery<T>): Future<T> = query(this)
