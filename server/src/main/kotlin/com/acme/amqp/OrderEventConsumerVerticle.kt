package com.acme.amqp

import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.rabbitmq.QueueOptions
import io.vertx.rabbitmq.RabbitMQClient
import io.vertx.rabbitmq.RabbitMQOptions
import org.apache.logging.log4j.kotlin.Logging

class OrderEventConsumerVerticle : CoroutineVerticle(), Logging {
    override suspend fun start() {
        /**
         * register a handler
         */
        val testBodyHandler = { body: JsonObject ->
            logger.info("received $body")
            if (body.containsKey("nack")) {
                Future.failedFuture("NACK on purpose")
            } else Future.succeededFuture(jsonObjectOf())
        }
        val config = RabbitMQOptions().apply {
            host = "localhost"
            port = 5671
            isAutomaticRecoveryEnabled = true
            reconnectAttempts = 0
        }

        /**
         * there's only one Channel instance per RabbitMQClient,
         * if we don't want to share the same channel, we need to create separate RabbitMQClients
         * Here we will have Connections equal to the number of Verticles
         */
        val rabbitMQClient = RabbitMQClient.create(vertx, config)
        rabbitMQClient.start().await()
        rabbitMQClient.consume("queue.orders", testBodyHandler)
    }


    private fun RabbitMQClient.consume(queue: String, bodyHandler: (JsonObject) -> Future<JsonObject>) {
        basicConsumer(queue, QueueOptions().setAutoAck(false)) { ar ->
            if (ar.failed()) {
                logger.error(ar.cause()) { "consumer registration failed for queue $queue" }
            } else {
                ar.result().handler { message ->
                    // processing message...
                    val toJson = message.body().toJsonObject()
                    bodyHandler(toJson).onSuccess {
                        basicAck(message.envelope().deliveryTag, false).onFailure {
                            logger.error(it) { "ACK K.O. $toJson" }
                        }.onSuccess {
                            logger.info { "ACKed $toJson" }
                        }
                    }.onFailure {
                        basicNack(message.envelope().deliveryTag, false, false).onFailure {
                            logger.error { "NACK K.O. $toJson" }
                        }.onSuccess {
                            logger.info { "NACKed $toJson" }
                        }
                    }
                }
            }
        }
    }
}
