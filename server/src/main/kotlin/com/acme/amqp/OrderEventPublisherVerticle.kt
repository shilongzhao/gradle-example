package com.acme.amqp

import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.rabbitmq.RabbitMQClient
import io.vertx.rabbitmq.RabbitMQOptions
import io.vertx.serviceproxy.ServiceBinder
import org.apache.logging.log4j.kotlin.Logging
import java.time.ZonedDateTime

class OrderEventPublisherVerticle: CoroutineVerticle(), Logging {

    override suspend fun start() {
        val config = RabbitMQOptions().apply {
            host = "localhost"
            port = 5671
            isAutomaticRecoveryEnabled = true
            reconnectAttempts = 0
        }

        val rabbitMQClient = RabbitMQClient.create(vertx, config)
        rabbitMQClient.start().await()

        ServiceBinder(vertx)
            .setAddress(OrderEventPublisherProxy.ADDRESS)
            .register(
                OrderEventPublisher::class.java,
                OrderEventPublisherImpl(rabbitMQClient)
            )
        logger.info { "Order event listener deployed" }
//        /*

        val orderEventPublisher = OrderEventPublisherProxy.createProxy(vertx, OrderEventPublisherProxy.ADDRESS)

        vertx.setPeriodic(10_000) {
            val message = jsonObjectOf("type" to "order", "timestamp" to ZonedDateTime.now().toString())
            orderEventPublisher.publishOrderEvent(message)
        }

//         */
    }
}
