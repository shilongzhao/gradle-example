package com.acme.amqp

import com.rabbitmq.client.AMQP
import io.vertx.core.Future
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.rabbitmq.RabbitMQClient
import io.vertx.rabbitmq.RabbitMQOptions
import org.apache.logging.log4j.kotlin.Logging

/**
 * consider using RabbitMQPublisher for reliable send
 */
class RabbitClientVerticle : CoroutineVerticle(), Logging {
    companion object {
        const val ordersExchange = "exchange.orders"
        const val ordersQueue = "queue.orders"
        const val routingKey = "routing.orders"
    }

    override suspend fun start() {
        val config = RabbitMQOptions().apply {
            host = "localhost"
            port = 5671
            isAutomaticRecoveryEnabled = true
            reconnectAttempts = 0
        }

        val client = RabbitMQClient.create(vertx, config)

        client.addConnectionEstablishedCallback {
            client.declareExchangeWithDeadLetter(ordersExchange, "direct")
                .compose {
                    client.declareQueuesWithDeadLetter(ordersQueue, ordersExchange)
                }
                .compose {
                    client.bindWithDeadLetter(ordersQueue, ordersExchange, routingKey)
                }
                .onComplete(it)
        }

        client.start().await()

        logger.info { "Rabbit OK" }

        vertx.deployVerticle({ OrderEventConsumerVerticle() }, deploymentOptionsOf(instances = 3)).await()
        vertx.deployVerticle({ OrderEventPublisherVerticle() }, deploymentOptionsOf()).await()
        /*
        not useful anymore, just close the connection and channel
         */
        client.stop().await()
    }

    /**
     * declare exchange and dead letter exchange
     */
    private fun RabbitMQClient.declareExchangeWithDeadLetter(name: String, type: String): Future<Void> {
        val deadLetterOrdersExchange = "${name}.dead_letter"

        val f = exchangeDeclare(name, type, true, false)
            .compose {
                exchangeDeclare(deadLetterOrdersExchange, "direct", true, false)
            }.onSuccess {
                logger.info { "exchange declared $name" }
            }.onFailure {
                logger.error(it) { "exchange declare KO $name" }
            }

        return f
    }

    /**
     * declare queues and dead letter queues
     */
    private fun RabbitMQClient.declareQueuesWithDeadLetter(
        name: String,
        exchange: String
    ): Future<AMQP.Queue.DeclareOk> {
        val deadLetterExchange = "${exchange}.dead_letter"

        val config = jsonObjectOf("x-dead-letter-exchange" to deadLetterExchange)
        val f = queueDeclare(name, true, false, false, config)
            .compose {
                queueDeclare("${name}.dead_letter", true, false, false)
            }.onSuccess {
                logger.info { "queue declared $name" }
            }.onFailure {
                logger.error(it) { "queue declare KO $name" }
            }

        return f
    }

    /**
     * declare a queue and queue.dead_letter
     */
    private fun RabbitMQClient.bindWithDeadLetter(queue: String, exchange: String, routingKey: String): Future<Void> {
        val deadLetterExchange = "${exchange}.dead_letter"
        val f = queueBind(queue, exchange, routingKey).compose {
            queueBind("${queue}.dead_letter", deadLetterExchange, routingKey)
        }.onSuccess {
            logger.info { "binding declared $queue $exchange $routingKey" }
        }
        return f
    }

}

