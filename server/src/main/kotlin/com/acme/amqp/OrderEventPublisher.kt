package com.acme.amqp

import com.rabbitmq.client.AMQP
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.codegen.annotations.VertxGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.rabbitmq.RabbitMQClient
import io.vertx.rabbitmq.RabbitMQPublisher
import org.apache.logging.log4j.kotlin.Logging

@ProxyGen
@VertxGen
interface OrderEventPublisher {
    fun publishOrderEvent(message: JsonObject): Future<Void>
}

object OrderEventPublisherProxy {
    const val ADDRESS = "rabbitmq.orders.publisher.eventbus"
    fun createProxy(vertx: Vertx, address: String): OrderEventPublisher {
          return OrderEventPublisherVertxEBProxy(vertx, address)
    }
}

class OrderEventPublisherImpl(private val rabbitMQClient: RabbitMQClient): OrderEventPublisher, Logging {

    override fun publishOrderEvent(message: JsonObject): Future<Void> {
        logger.info { "order event $message" }
        return rabbitMQClient.basicPublish("exchange.orders", "routing.orders",  message.toBuffer())
    }

}
