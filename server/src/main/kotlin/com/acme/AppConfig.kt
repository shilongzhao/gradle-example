package com.acme

import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.pgclient.pgConnectOptionsOf
import io.vertx.pgclient.PgConnectOptions

object AppConfig {
    suspend fun appConfig(vertx: Vertx): JsonObject {
        val environment = System.getenv("APP_ENV") ?: "local"
        val fileStore = ConfigStoreOptions().apply {
            type = "file"
            isOptional = true
            config = json { obj("path" to "config-$environment.json") }
        }

        // environment overrides file config
        val envStore = ConfigStoreOptions().apply {
            type = "env"
        }

        // system override environment
        // java -Dserver.host=localhost -Dserver.port=8080 -jar applicaiton.jar
        val sysStore = ConfigStoreOptions().apply {
            type = "sys"
        }

        val configRetriever = ConfigRetriever.create(
            vertx,
            ConfigRetrieverOptions().addStore(fileStore).addStore(envStore).addStore(sysStore)
        )

        return configRetriever.config.await()
    }

    suspend fun getConnectionOptions(config: JsonObject): PgConnectOptions {
        val databaseConfig = config.getJsonObject("database")

        return pgConnectOptionsOf(
            host = databaseConfig.getString("host"),
            port = databaseConfig.getInteger("port"),
            user = databaseConfig.getString("username"),
            password = databaseConfig.getString("password"),
            database = databaseConfig.getString("database")
        )
    }
}
