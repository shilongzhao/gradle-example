package com.acme.http

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import com.acme.RoutingContextHandler
import com.acme.database.pojo.BankAccount
import com.acme.database.pojo.jsonString
import com.acme.database.service.BankAccountRepo
import com.acme.database.service.BankAccountRepoProxy
import com.acme.jsonString
import com.acme.util.either
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST
import io.netty.handler.codec.http.HttpResponseStatus.OK
import io.vertx.core.Context
import io.vertx.core.Vertx
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.micrometer.backends.BackendRegistries
import io.vertx.serviceproxy.ServiceException
import kotlinx.coroutines.launch
import org.apache.logging.log4j.kotlin.Logging

class HttpServerVerticle: CoroutineVerticle(), Logging {

    private lateinit var bankAccountRepo: BankAccountRepo
    private lateinit var registry: PrometheusMeterRegistry

    override fun init(vertx: Vertx, context: Context) {
        super.init(vertx, context)

        registry = BackendRegistries.getDefaultNow() as PrometheusMeterRegistry
        bankAccountRepo = BankAccountRepoProxy.createProxy(vertx)

    }
    override suspend fun start() {

        val router = Router.router(vertx)
        router.apply {
            route().apply {
                handler(LoggerHandler.create())
                handler(BodyHandler.create().setBodyLimit(1024 * 1024L))
            }

            get("/metrics", metricsHandler)
            get("/accounts/:accountId", getAccountHandler)

        }
        vertx.createHttpServer().requestHandler(router).listen(8088).await()

    }

    private val metricsHandler: RoutingContextHandler = { ctx ->
        ctx.response().end(registry.scrape())
    }

    private val getAccountHandler: RoutingContextHandler = { ctx ->
        val accountId = ctx.pathParam("accountId")

        val getByIdResultTranslator = { either: Either<Throwable, BankAccount> ->
            when(either) {
                is Right -> Pair(OK.code(), either.value.jsonString)
                is Left -> Pair(BAD_REQUEST.code(), (either.value as ServiceException).jsonString)
            }
        }
        val result = bankAccountRepo.getById(accountId).either()
        val (code, body) = getByIdResultTranslator(result)

        ctx.response().setStatusCode(code).end(body)
    }

    /* until function to avoid writing 'coroutineHandler's for every get request */
    private fun Router.get(path: String, handler: RoutingContextHandler) =
        this.get(path).coroutineHandler(handler)

    /* so that we can use suspend functions in route handlers */
    private fun Route.coroutineHandler(fn: RoutingContextHandler) =
        handler {
            // we can use `launch` here, because we are in side a CoroutineScope,
            // our verticle extends CoroutineVerticle which in turn implements CoroutineScope,
            // thus here we are inside it (but our verticle also has a coroutineScope as context)
            launch {
                try {
                    fn(it)
                } catch (e: Exception) {
                    when(e) {
                        is RuntimeException -> it.fail(500, e)
                        else -> it.fail(400, e)
                    }
                }
            }
        }

}
