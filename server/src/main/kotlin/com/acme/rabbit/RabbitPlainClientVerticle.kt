package com.acme.rabbit

import com.acme.rabbit.RPCClientVerticle.Companion.RPC_EXCHANGE
import com.acme.rabbit.RPCClientVerticle.Companion.RPC_QUEUE
import com.rabbitmq.client.ConnectionFactory
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.withContext
import org.apache.logging.log4j.kotlin.Logging

/**
 * try to implement RMQ functionalities directly with RMQ client lib without vertx-rabbitmq-client
 */
class RabbitPlainClientVerticle : Logging, CoroutineVerticle() {

    override suspend fun start() {
        val factory = ConnectionFactory()
        factory.apply {
            host = "localhost"
            port = 5671
            username = "guest"
            password = "guest"
        }


        val connection = withContext(vertx.dispatcher()) {
            runCatching {
                val connection = factory.newConnection("plain-verticle-rpc-connection")
                val channel = connection.createChannel()
                channel.exchangeDeclare(RPC_EXCHANGE, "direct", true, false, mapOf())

                channel.queueDeclare(RPC_QUEUE, true, false, false, mapOf())

                channel.queueBind(RPC_QUEUE, RPC_EXCHANGE, RPC_QUEUE)

                channel.close()

                connection
            }
        }.getOrThrow()


        vertx.deployVerticle({ RPCClientVerticle(connection) }, deploymentOptionsOf(instances = 2)).await()
        vertx.deployVerticle({ RPCServerVerticle(connection) }, deploymentOptionsOf(instances = 2)).await()

        logger.info { "deploy OK" }
    }
}
