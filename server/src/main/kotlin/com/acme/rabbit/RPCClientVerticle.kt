package com.acme.rabbit

import com.rabbitmq.client.*
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.withContext
import org.apache.logging.log4j.kotlin.Logging
import java.util.*
import java.util.concurrent.ArrayBlockingQueue

class RPCClientVerticle(val connection: Connection) : CoroutineVerticle(), Logging {
    companion object {
        const val RPC_EXCHANGE = "exchange.rpc"
        const val RPC_QUEUE = "queue.rpc"
    }

    override suspend fun start() {

        /**
         * separate channel for each client
         */
        val channel = withContext(vertx.dispatcher()) {
            runCatching {
                connection.createChannel()
            }
        }.getOrThrow()

        vertx.setPeriodic(10_000) {
            channel.call(jsonObjectOf("id" to it, "thread" to Thread.currentThread().name, "type" to "rpc"))
        }

    }

    private fun Channel.call(json: JsonObject): JsonObject {
        val correlationId = UUID.randomUUID().toString()

        val callbackQueue = queueDeclare().queue

        val persistent = 2
        val properties = AMQP.BasicProperties.Builder()
            .deliveryMode(persistent)
            .correlationId(correlationId)
            .contentType("application/json")
            .replyTo(callbackQueue)
            .build()

        basicPublish(RPC_EXCHANGE, RPC_QUEUE, properties, json.toString().toByteArray())
        
        val response = ArrayBlockingQueue<JsonObject>(1)
        val deliverCallback = DeliverCallback { _, message ->
            val body = String(message.body)
            logger.info { "received response: $body" }
            if (message.properties.correlationId == correlationId) {
                response.offer(JsonObject(body))
            }
        }
        val cancelCallback = CancelCallback { consumerTag ->
            logger.info { "consumer $consumerTag cancelled" }
        }

        val consumerId = basicConsume(callbackQueue, true, deliverCallback, cancelCallback)
        // delete consumer
        return response.take().also {
            // first take result and in the end cancel the consumer
            basicCancel(consumerId)
        }
    }
}
