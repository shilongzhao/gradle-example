package com.acme.rabbit

import com.acme.rabbit.RPCClientVerticle.Companion.RPC_QUEUE
import com.rabbitmq.client.*
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.withContext
import org.apache.logging.log4j.kotlin.Logging
import java.time.ZonedDateTime

class RPCServerVerticle(val connection: Connection) : CoroutineVerticle(), Logging {

    private val deliveryCallbackHandler: (Channel) -> DeliverCallback = { channel ->
        DeliverCallback { _, message ->
            val replyProperties = AMQP.BasicProperties.Builder()
                .correlationId(message.properties.correlationId)
                .build()
            val request = JsonObject(String(message.body))
            logger.info("received request $request")
            val response = jsonObjectOf("id" to request.getLong("id"), "timestamp" to ZonedDateTime.now().toString())
            channel.basicPublish(
                "",
                message.properties.replyTo,
                replyProperties,
                response.encode().toByteArray()
            )
            // manual ACK
            channel.basicAck(message.envelope.deliveryTag, false)
        }
    }

    private val cancelCallback = CancelCallback { consumerTag ->
        logger.info { "consumer $consumerTag cancelled" }
    }

    override suspend fun start() {

        val serverConsumer = withContext(vertx.dispatcher()) {
            runCatching {
                val channel = connection.createChannel()
                logger.info { "registering consumers for $RPC_QUEUE" }
                // set autoAck as false, manually ACK in the deliverCallback
                channel.basicConsume(RPC_QUEUE, false, deliveryCallbackHandler(channel), cancelCallback)
            }
        }.getOrThrow()
        logger.info { "RPC server ready: $serverConsumer" }
    }
}
