package com.acme.metrics

import io.vertx.micrometer.backends.BackendRegistries
import java.lang.RuntimeException

inline fun <T> timed(timer: String, crossinline block: () -> T): T =
    BackendRegistries.getDefaultNow().timer(timer, listOf()).recordCallable {
        block()
    } ?: throw RuntimeException()

