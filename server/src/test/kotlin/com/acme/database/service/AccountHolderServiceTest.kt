package com.acme.database.service

import com.acme.AppConfig
import com.acme.database.pojo.AccountHolder
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.pgclient.PgPool
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

/*
unit test without deploying any verticle here
 */
@ExtendWith(VertxExtension::class)
class AccountHolderServiceTest: Logging {
    companion object {
        private lateinit var accountHolderRepo: AccountHolderRepo
        private lateinit var pgPool: PgPool
        @BeforeAll @JvmStatic
        fun startup(vertx: Vertx): Unit = runBlocking {
            pgPool = PgPool.pool(vertx, AppConfig.getConnectionOptions(AppConfig.appConfig(vertx)), poolOptionsOf())
            accountHolderRepo = AccountHolderRepoImpl(vertx, pgPool)
        }

        @JvmStatic @AfterAll
        fun cleanup(): Unit = runBlocking {
            pgPool.close().await()
        }
    }

    @BeforeEach
    fun clear(): Unit = runBlocking {
        pgPool.query("TRUNCATE bank_account CASCADE; TRUNCATE account_holder CASCADE;")
            .execute().await()
    }


    @Test
    fun `create a holder and later get by id should be OK`() {
        runBlocking {
            val accountHolder = AccountHolder(UUID.randomUUID(), "shiro", "shilong", "zhao")
            val id = accountHolderRepo.insert(accountHolder).await()
            logger.debug {"inserted holder $id: $accountHolder"}

            val saved = accountHolderRepo.getById(id).await()

            assertEquals("shiro", saved.username)
            assertEquals("shilong", saved.firstname)
            assertEquals("zhao", saved.lastname)
        }
    }
}
