package com.acme.database.service

import com.acme.AppConfig
import com.acme.database.PostgresClientVerticle
import com.acme.database.pojo.AccountHolder
import com.acme.database.pojo.BankAccount
import com.acme.database.pojo.BankAccountType
import com.acme.util.uuid
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.pgclient.PgPool
import io.vertx.serviceproxy.ServiceProxyBuilder
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.ExtendWith
import java.math.BigDecimal
import java.util.*

@ExtendWith(VertxExtension::class)
class BankAccountServiceVerticleTest : Logging {

    companion object {
        private lateinit var pgPool: PgPool
        private lateinit var bankAccountRepo: BankAccountRepo

        @JvmStatic
        @BeforeAll
        fun startup(vertx: Vertx): Unit = runBlocking {
            bankAccountRepo =
                ServiceProxyBuilder(vertx).setAddress(BankAccountRepoProxy.ADDRESS).build(BankAccountRepo::class.java)
            val config = AppConfig.appConfig(vertx)

            val connectionOptions = AppConfig.getConnectionOptions(config)
            pgPool = PgPool.pool(vertx, connectionOptions, poolOptionsOf())
            vertx.deployVerticle({ PostgresClientVerticle() }, deploymentOptionsOf(config = config)).await()
        }

        @JvmStatic
        @AfterAll
        fun close(): Unit = runBlocking {
            pgPool.close().await()
        }
    }

    @BeforeEach
    fun clear(): Unit = runBlocking {
        pgPool.query("TRUNCATE bank_account CASCADE; TRUNCATE account_holder CASCADE;")
            .execute().await()
    }


    @Test
    fun `create bank account should be OK`(vertx: Vertx) = runBlocking {
        val holder = AccountHolder(UUID.randomUUID(), "shiro", "shilong", "zhao")
        val holderId = pgPool.execute(AccountHolderQueries.insert(holder)).await()

        logger.debug { "inserted holder ${holder.id}: $holder" }
        // in DB it's set Numeric(precision=12, scale = 2)
        val request = BankAccount(UUID.randomUUID(), holderId.uuid, BankAccountType.SAVINGS, BigDecimal("0.00"), 1L)
        val accountId = bankAccountRepo.insert(request).await()
        Assertions.assertNotNull(accountId)

        val saved = bankAccountRepo.getById(accountId).await()
        assertEquals(request, saved)
    }

}
